const mongoose = require("mongoose");
const { Schema } = mongoose;

module.exports = mongoose.model(
  "note",
  new Schema({
    userId: {
      required: true,
      type: Schema.Types.ObjectId,
      ref: "User",
    },
    text: {
      required: true,
      type: String,
    },
    completed: {
      type: Boolean,
      default: false,
    },
    createdDate: {
      type: Date,
      default: Date.now,
    },
  })
);
