const crypto = require('crypto');

module.exports.createHash = function (password) {
  return crypto.createHash('sha256').update(password).digest('base64');
}

module.exports.compareHashes = function (password, hash) {
  let hashed = crypto.createHash('sha256').update(password).digest('base64');
  return hashed === hash;
}
