const User = require("../models/user");
const { createHash, compareHashes } = require('../utils/crypto');

module.exports.getProfileInfo = (request, response) => {
  User.findById(request.user._id)
    .exec()
    .then((user) => {
      if (!user) {
        return response.status(400).json({ message: "User does not exist" });
      }
      response.status(200).json({
        user: {
          _id: user._id,
          username: user.username,
          createdDate: user.createdDate,
        },
      });
    })
    .catch((err) => {
      response.status(500).json({ status: err.message });
    });
};

module.exports.deleteProfile = (request, response) => {
  User.findById(request.user._id)
    .exec()
    .then((user) => {
      if (!user) {
        return response.status(400).json({ message: "User does not exist" });
      }
      user
        .remove()
        .then(response.status(200).json({ message: "Success" }))
        .catch((err) => {
          response.status(500).json({ status: err.message });
        });
    })
    .catch((err) => {
      response.status(500).json({ message: err.message });
    });
};

module.exports.changeProfilePassword = (request, response) => {
  const { oldPassword, newPassword } = request.body;

  if (!oldPassword) {
    return response
      .status(400)
      .json({ message: "Please, provide old password" });
  }
  if (!newPassword) {
    return response
      .status(400)
      .json({ message: "Please, provide new password" });
  }

  User.findById(request.user._id)
    .then((user) => {
      if (!user) {
        response.status(400).json({ message: "User does not exist" });
        return;
      }
      const oldPasswordFromDB = user.password;
      if (!compareHashes(oldPassword, oldPasswordFromDB)) {
        response.status(400).json({ message: "Incorrect old password" });
      } else {
        const hashedNewPass = createHash(newPassword);
        User.findByIdAndUpdate(request.user._id, { password: hashedNewPass })
          .exec()
          .then((user) => {
            response.status(200).json({ message: "Success" });
          })
          .catch((err) => {
            response.status(500).json({ message: err.message });
          });
      }
    })
    .catch((err) => {
      response.status(500).json({ message: err.message });
    });
};
