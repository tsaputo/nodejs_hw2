const Note = require("../models/note");
const mongoose = require('mongoose');

module.exports.getUserNoteById = (request, response) => {
  const { note_id } = request.params;
  let isValid = mongoose.Types.ObjectId.isValid(note_id);
  if (!isValid) {
    return response
        .status(400)
        .json({
          message:
            "Id format is not valid",
        });
  }
  Note.findById({ _id: note_id })
    .where({ userId: request.user._id })
    .exec()
    .then((note) => {
      if (!note) {
        return response
          .status(400)
          .json({
            message:
              "There is no note with such id or you do not have access to it",
          });
      }
      response.status(200).json({
        note: {
          _id: note._id,
          userId: note.userId,
          completed: note.completed,
          text: note.text,
          createdDate: note.createdDate,
        },
      });
    })
    .catch((err) => {
      response.status(500).json({ message: err.message });
    });
};

module.exports.deleteUserNoteById = (request, response) => {
  const { note_id } = request.params;
  let isValid = mongoose.Types.ObjectId.isValid(note_id);
  if (!isValid) {
    return response
        .status(400)
        .json({
          message:
            "Id format is not valid",
        });
  }
  const query = Note.where({ userId: request.user._id });
  query
    .findOne({ _id: note_id })
    .exec()
    .then((note) => {
      if (!note) {
        return response
          .status(400)
          .json({ status: "There is no note with such id! Try again!" });
      }
      note.remove();
      response.status(200).json({ message: "Success" });
    })
    .catch((err) => {
      response.status(400).json({ message: err.message });
    });
};

module.exports.getUserNotes = (request, response) => {
  Note.find()
    .where({ userId: request.user._id })
    .exec()
    .then((notes) => {
      if (!notes) {
        return response.status(400).json({ message: "You have no notes yet" });
      }
      response.status(200).json({
        notes: [...notes].map((note) => {
          return {
            _id: note._id,
            userId: note.userId,
            completed: note.completed,
            text: note.text,
            createdDate: note.createdDate,
          };
        }),
      });
    })
    .catch((err) => {
      response.status(500).json({ message: err.message });
    });
};

module.exports.addNote = (request, response) => {
  const { text } = request.body;
  const userId = request.user._id;
  const note = new Note({ text, userId });
  note
    .save()
    .then(() => {
      response.status(200).json({ message: "Success" });
    })
    .catch((error) => {
      response.status(400).json({ message: error.message });
    });
};

module.exports.updateUserNoteById = (request, response) => {
  const { note_id } = request.params;
  const { text } = request.body;
  let isValid = mongoose.Types.ObjectId.isValid(note_id);
  if (!isValid) {
    return response
        .status(400)
        .json({
          message:
            "Id format is not valid",
        });
  }
  if (!text) {
    return response
      .status(400)
      .json({ message: "Please, pass message text into body of response" });
  }

  Note.findById({ _id: note_id })
    .where({ userId: request.user._id })
    .exec()
    .then((note) => {
      if (!note) {
        return response
          .status(400)
          .json({ message: "There is no note with such id!" });
      }
      note
        .updateOne({ text: text })
        .exec()
        .then((note) => {
          response.status(200).json({ message: "Success" });
        })
        .catch((err) => {
          response.status(500).json({ message: err.message });
        });
    })
    .catch((err) => {
      response.status(500).json({ message: err.message });
    });
};

module.exports.toggleCompletedForUserNoteById = (request, response) => {
  const { note_id } = request.params;
  let isValid = mongoose.Types.ObjectId.isValid(note_id);
  if (!isValid) {
    return response
        .status(400)
        .json({
          message:
            "Id format is not valid",
        });
  }
  Note.findById({ _id: note_id })
    .where({ userId: request.user._id })
    .exec()
    .then((note) => {
      if (!note) {
        return response
          .status(400)
          .json({ message: "There is no note with such id!" });
      }
      note.completed = !note.completed;
      note.save();
      response.status(200).json({ message: "Success" });
    })
    .catch((err) => {
      response.status(500).json({ message: err.message });
    });
};
