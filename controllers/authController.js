const jwt = require("jsonwebtoken");
const User = require("../models/user");
const { secret } = require("../config/auth");
const { createHash } = require('../utils/crypto');

module.exports.register = (request, response) => {
  const { username, password } = request.body;

  if (!username || !password) {
    response.status(400).json({ message: "Required params missing" });
    return;
  }

  User.findOne({ username })
    .exec()
    .then((user) => {
      if (user) {
        response.status(400).json({ message: "Username already exists" });
        return;
      }

      const hash = createHash(password);
      const newUser = new User({ username: username, password: hash });
      newUser
        .save()
        .then(() => {
          response.status(200).json({ message: "Success" });
        })
        .catch((error) => {
          response.status(500).json({ message: error.message });
        });
    });
};

module.exports.login = (request, response) => {
  const { username, password } = request.body;

  if (!username) {
    response.status(400).json({ message: "Username is missing. Please, provide username" });
    return;
  }

  if (!password) {
    response.status(400).json({ message: "Password is missing. Please, enter your password" });
    return;
  }

  const hash = createHash(password);
  User.findOne({ username: username, password: hash })
    .exec()
    .then((user) => {
      if (!user) {
        return response
          .status(400)
          .json({ message: "Wrong username or password" });
      }
      response
        .status(200)
        .json({ jwt_token: jwt.sign(JSON.stringify(user), secret) });
    })
    .catch((err) => {
      response.status(500).json({ message: err.message });
    });
};
