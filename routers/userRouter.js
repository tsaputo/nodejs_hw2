const express = require("express");
const router = express.Router();
const authMiddleware = require("../middlewares/authMiddleware");

const {
  getProfileInfo,
  deleteProfile,
  changeProfilePassword,
} = require("../controllers/userController");

router.get("/me", authMiddleware, getProfileInfo);
router.delete("/me", authMiddleware, deleteProfile);
router.patch("/me", authMiddleware, changeProfilePassword);

module.exports = router;
