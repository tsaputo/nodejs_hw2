const express = require("express");
const router = express.Router();
const authMiddleware = require("../middlewares/authMiddleware");

const {
  getUserNotes,
  addNote,
  getUserNoteById,
  deleteUserNoteById,
  updateUserNoteById,
  toggleCompletedForUserNoteById,
} = require("../controllers/noteController");

router.get("/notes/:note_id", authMiddleware, getUserNoteById);
router.get("/notes", authMiddleware, getUserNotes);
router.post("/notes", authMiddleware, addNote);
router.put("/notes/:note_id", authMiddleware, updateUserNoteById);
router.patch("/notes/:note_id", authMiddleware, toggleCompletedForUserNoteById);
router.delete("/notes/:note_id", authMiddleware, deleteUserNoteById);

module.exports = router;
