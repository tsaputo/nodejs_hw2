import React, { useEffect, useState } from "react";
import { Router } from "react-router-dom";
import { history } from "./utils/history";
import Routes from "./components/router";
import HeaderComponent from "./components/header";

const App = () => {
  const [currentRoute, setCurrentRoute] = useState(history.location);

  const historyChangeUnsubscribe = history.listen((location) => {
    setCurrentRoute(location);
  });

  useEffect(() => historyChangeUnsubscribe());

  return (
    <div className="app-container">
      <Router history={history}>
        <HeaderComponent />
        <main className="main-content-wr">
          <Routes />
        </main>
      </Router>
    </div>
  );
};

export default App;
