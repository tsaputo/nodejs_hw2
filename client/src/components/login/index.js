import React from "react";
import { useForm } from "react-hook-form";
import styles from "./login.module.scss";

const LoginComponent = () => {
  const { register, handleSubmit } = useForm();
  const onSubmit = (data) => {
    const requestOptions = {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        username: data.username,
        password: data.password,
      }),
    };
    fetch("http://localhost:8080/api/auth/login", requestOptions)
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        localStorage.setItem("token", data.jwt_token);
      });
  };

  return (
    <>
      <form className={styles.container} onSubmit={handleSubmit(onSubmit)}>
        <label>
          Username:
          <input name="username" ref={register} />
        </label>
        <label>
          Password:
          <input name="password" ref={register} />
        </label>
        <button type="submit">Login</button>
      </form>
    </>
  );
};

export default LoginComponent;
