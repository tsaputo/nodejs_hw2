import React from "react";
import NoteComponent from "../note";
import styles from "./noteItems.module.scss";

const NoteItemsComponent = (props) => {
  return (
    <div className={styles.container}>
      {props.data.map((note) => (
        <NoteComponent key={note.id} note={note} />
      ))}
    </div>
  );
};

export default NoteItemsComponent;
