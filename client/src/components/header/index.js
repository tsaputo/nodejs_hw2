import React from "react";
import styles from "./header.module.scss";
import NavigationComponent from "../navigation";

const HeaderComponent = () => {
  return (
    <header className={styles.container}>
      <NavigationComponent />
    </header>
  );
};

export default HeaderComponent;
