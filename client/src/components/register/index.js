import React, { useEffect } from "react";
import { useForm } from "react-hook-form";
import styles from "./register.module.scss";

const RegisterComponent = () => {
  const { register, handleSubmit } = useForm();
  const onSubmit = (data) => {
    const requestOptions = {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        username: data.username,
        password: data.password,
      }),
    };
    fetch(
      "http://localhost:8080/api/auth/register",
      requestOptions
    ).then((response) => response.json());
  };

  return (
    <div>
      <form className={styles.container} onSubmit={handleSubmit(onSubmit)}>
        <label>
          Username:
          <input name="username" ref={register} />
        </label>
        <label>
          Password:
          <input name="password" ref={register} />
        </label>
        <button type="submit">Register</button>
      </form>
    </div>
  );
};

export default RegisterComponent;
