import React from "react";
import styles from "./note.module.scss";

const NoteComponent = (props) => {
  return (
    <div className={styles.container}>
      <div className={styles.text}>{props.note.text}</div>
      <div className={styles.created}>
        {props.note.createdDate.slice(0, 10)}
      </div>
      <input type="checkbox" checked={props.note.completed}></input>
      <div className={styles.buttonContainer}>
        <button>Edit</button>
        <button>Delete</button>
      </div>
    </div>
  );
};

export default NoteComponent;
