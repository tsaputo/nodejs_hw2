import styles from "./profile.module.scss";

const ProfileComponent = (props) => {
  return (
    <div className={styles.container}>
      <h2>{`username: ${props.data}`}</h2>
    </div>
  );
};

export default ProfileComponent;
