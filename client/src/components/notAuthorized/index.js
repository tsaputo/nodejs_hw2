import styles from "./notAuthorized.module.scss";

const notAuthorizedComponent = () => {
  return (
    <div className={styles.container}>
      <h2>You are not authorized to see this page. Please, log in.</h2>
    </div>
  );
};

export default notAuthorizedComponent;
