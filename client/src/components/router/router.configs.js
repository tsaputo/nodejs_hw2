import React, { lazy } from "react";

const User = lazy(() => import("../../containers/User"));
const Notes = lazy(() => import("../../containers/Notes"));
const Register = lazy(() => import("../../containers/Register"));

export const routes = [
  {
    id: "user",
    path: "/me",
    component: User,
    title: "User",
  },
  {
    path: "/notes",
    component: Notes,
    title: "Notes",
  },
  {
    path: "/register",
    component: Register,
    title: "Register/login",
  },
];
