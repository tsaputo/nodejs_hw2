import React, { Suspense } from "react";
import { history } from "../../utils/history";
import { Router, Route } from "react-router-dom";
import { routes } from "./router.configs";

const Routes = () => {
  return (
    <Router history={history}>
      <Suspense fallback={<div>Loading...</div>}>
        {routes.map((r, index) => (
          <Route key={index} exact path={r.path} component={r.component} />
        ))}
      </Suspense>
    </Router>
  );
};

export default Routes;
