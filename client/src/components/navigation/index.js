import { Link } from "react-router-dom";
import React from "react";
import { routes } from "../router/router.configs";
import styles from "./navigation.module.scss";

const NavigationComponent = () => {
  const menuItems = routes.filter((r) => r.id !== "notes");
  return (
    <ul className={styles.list}>
      {menuItems.map((r) => (
        <li key={r.title} className={styles.item}>
          <Link to={r.path}>{r.title}</Link>
        </li>
      ))}
    </ul>
  );
};

export default NavigationComponent;
