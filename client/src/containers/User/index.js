import React, { useState, useEffect } from "react";
import ProfileComponent from "../../components/profile";
import NotAuthorizedComponent from "../../components/notAuthorized";

const User = () => {
  const [data, setData] = useState();
  const [status, setStatus] = useState();

  useEffect(() => {
    const fetchData = async () => {
      const requestOptions = {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          authorization: `${localStorage.getItem("token")}`,
        },
      };
      fetch("http://localhost:8080/api/users/me", requestOptions)
        .then((response) => {
          setStatus(response.status);
          return response.json();
        })
        .then((data) => {
          setData(data.user.username);
        })
        .catch((err) => {
          console.log(err);
        });
    };
    fetchData();
  }, []);

  let layout;
  if (status === 401) {
    layout = <NotAuthorizedComponent />;
  } else {
    layout = <ProfileComponent data={data} />;
  }

  return <div>{layout}</div>;
};

export default User;
