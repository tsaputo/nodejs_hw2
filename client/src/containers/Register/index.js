import React from "react";
import LoginComponent from "../../components/login";
import RegisterComponent from "../../components/register";
import styles from "./register.module.scss";

const Register = (props) => {
  return (
    <div className={styles.container}>
      <RegisterComponent />
      <LoginComponent />
    </div>
  );
};

export default Register;
