import React, { useState, useEffect } from "react";
import NoteItemsComponent from "../../components/noteItems";
import NotAuthorizedComponent from "../../components/notAuthorized";

const Notes = () => {
  const [data, setData] = useState([]);
  const [status, setStatus] = useState();

  useEffect(() => {
    const fetchData = async () => {
      const requestOptions = {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          authorization: `${localStorage.getItem("token")}`,
        },
      };
      fetch("http://localhost:8080/api/notes", requestOptions)
        .then((response) => {
          setStatus(response.status);
          return response.json();
        })
        .then((data) => {
          if (data.length > 0) {
            const values = Object.values(data.notes);
            const arr = [];
            values.forEach((value) => {
              let item = {};
              item.id = value.id;
              item.text = value.text;
              item.createdDate = value.createdDate;
              item.completed = value.completed;
              arr.push(item);
            });
            setData(arr);
          }
        });
    };
    fetchData();
  }, []);

  let layout;
  if (status === 401) {
    layout = <NotAuthorizedComponent />;
  } else {
    layout = <NoteItemsComponent data={data} />;
  }

  return <div>{layout}</div>;
};

export default Notes;
