const auth = require("../config/auth");
const jwt = require("jsonwebtoken");
const { secret } = require("../config/auth");
const user = require("../models/user");

module.exports = (request, response, next) => {
  const token = request.headers["authorization"];

  if (!token) {
    return response
      .status(401)
      .json({ status: "No authorization header found" });
  }

  try {
    payload = jwt.verify(token, secret);
    user.findById(payload._id, function (err, user) {
      if (err) {
        response.status(500).json({ status: err.message });
      }
      if (!user) {
        response.status(401).json({ status: "User doesn't exist. Deleted?" });
      } else {
        request.user = user;
        next();
      }
    });
  } catch (err) {
    return response.status(401).json({ status: "Invalid JWT" });
  }
};
