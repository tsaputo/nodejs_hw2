const express = require("express");
const mongoose = require("mongoose");

const app = express();
var cors = require("cors");

const { port } = require("./config/server");
const dataBaseConfig = require("./config/database");

const userRouter = require("./routers/userRouter");
const authRouter = require("./routers/authRouter");
const noteRouter = require("./routers/noteRouter");

mongoose.connect(
  `mongodb+srv://${dataBaseConfig.username}:${dataBaseConfig.password}@${dataBaseConfig.host}/${dataBaseConfig.databaseName}?retryWrites=true&w=majority`,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
  }
);

app.use(cors());
app.use(express.json());
app.use("/api/users", userRouter);
app.use("/api", noteRouter);
app.use("/api", authRouter);

app.listen(port, () => {
  console.log(`app is listening on ${port} port`);
});
